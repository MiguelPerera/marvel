//
//  ContentView.swift
//  Marvel
//
//  Created by Miguel Angel Perera-Becerra on 11/25/19.
//  Copyright © 2019 MAP. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
